package main

import "fmt"

func main() {
	c := make(chan string)

	go func() {
		// goroutine 1
		c <- "hello!"
	}()

	// goroutine 2
	s := <-c
	fmt.Println(s) // "hello!"

}
