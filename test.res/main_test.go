package main

import "testing"

func TestAdd(t *testing.T) {
	res := add(2, 2)
	if res != 4 {
		t.Error("We failed")
	}
}

func BenchmarkAdd(b *testing.B) {
	for x := 0; x < b.N; x++ {
		y := add(2,x)
		add(x,y)
	}
}
