package main

import (
	"encoding/json"
	"encoding/xml"
	"log"
	"net/http"
)

type Person struct {
	Name    string
	Age     int
	Hobbies []string
}

// START OMIT
func createPerson() Person {
	return Person{Name: "Soren", Age: 18, Hobbies: []string{"Go", "Beer"}}
}
func handleJSON(w http.ResponseWriter, r *http.Request) {
	person := createPerson()
	if err := json.NewEncoder(w).Encode(person); err != nil {
		panic(err)
	}
}

func handleXML(w http.ResponseWriter, r *http.Request) {
	person := createPerson()
	if err := xml.NewEncoder(w).Encode(person); err != nil {
		panic(err)
	}
}

func main() {
	http.HandleFunc("/json", handleJSON)
	http.HandleFunc("/xml", handleXML)
	log.Fatal(http.ListenAndServe(":8080", nil))
}

// END OMIT
