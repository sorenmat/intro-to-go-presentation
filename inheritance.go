package main

import "fmt"

type Person struct {
     Name string
}

type Morton struct {
     Person
     minions int
}


func main() {
	morten := Morton{Person: Person{ Name: "Morten"}, minions: 45}
	fmt.Println(morten)
}
